package com.galvanize;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class ZipCodeProcessorTest {
    
    ZipCodeProcessor zipCodeProcessor;
    Verifier verifier = new Verifier();

    @BeforeEach
    void setVerifier() {
        zipCodeProcessor = new ZipCodeProcessor(verifier);
    }
    // write your tests here
    @Test
    public void assertTrue() {
        assertEquals(true, true);
    }

    @Test
    void processShouldReturnCorrectErrorMessageBasedOnPassedThroughZipCode() throws Exception {
        String successMsg = "Thank you!  Your package will arrive soon.";
        String wrongLengthMsg = "The zip code you entered was the wrong length.";
        String outOfRangeMsg = "We're sorry, but the zip code you entered is out of our range.";

        assertEquals(successMsg, zipCodeProcessor.process("80022"));
        assertEquals(wrongLengthMsg, zipCodeProcessor.process("2345678"));
        assertEquals(wrongLengthMsg, zipCodeProcessor.process("321"));
        assertEquals(outOfRangeMsg, zipCodeProcessor.process("12234"));
    }
    
}